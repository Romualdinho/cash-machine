﻿

namespace CashMachine
{
    using Binance.Net;
    using Binance.Net.Enums;
    using Binance.Net.Objects;
    using Binance.Net.Objects.Spot.SpotData;
    using CashMachine.Models;
    using CryptoExchange.Net.Authentication;
    using CryptoExchange.Net.Objects;
    using Microsoft.Extensions.Logging;

    public class CashMachineRunner
    {
        // Production state (use binance testnet if set to false)
        public const bool Production = true;

        // Immutables data related fields
        private readonly List<Allocation> allocations;
        private readonly List<Trade> trades;

        // Immutable Binance.NET related fields
        private readonly BinanceClient client;
        private readonly BinanceSocketClient socket;

        private readonly IList<Task> tasks = new List<Task>();

        private static readonly object consoleLocker = new object();

        private CashMachineRunner(ApiCredentials credentials, List<Allocation> allocations, List<Trade> trades)
        {
            if (credentials is null)
            {
                throw new ArgumentNullException(nameof(credentials));
            }

            if (allocations is null)
            {
                throw new ArgumentNullException(nameof(allocations));
            }

            if (trades is null)
            {
                throw new ArgumentNullException(nameof(trades));
            }

            this.allocations = allocations;
            this.trades = trades;

            // Rest API Client
            client = new BinanceClient(new BinanceClientOptions()
            {
                ApiCredentials = credentials,
                BaseAddress = Production ? "https://api.binance.com" : "https://testnet.binance.vision",
                LogLevel = LogLevel.Debug,
            });

            // WebSocket Client
            socket = new BinanceSocketClient(new BinanceSocketClientOptions()
            {
                ApiCredentials = credentials,
                AutoReconnect = true,
                LogLevel = LogLevel.Debug,
            });
        }

        public async Task RunAsync()
        {
            WriteLine($"CachMachine is now running! There are {trades.Count} trades to execute.");

            WriteSeperator();

            foreach (var trade in trades)
            {
                WriteLine($"[{trade.Id}] Buy {trade.SourceQuantity} {trade.SourceTicker} worth of {trade.TargetTicker} at a maximum price of {trade.MaxBuyPrice} {trade.SourceTicker} per {trade.TargetTicker}.");
                WriteLine($"[{trade.Id}] Resell all {trade.TargetTicker} purchased at {trade.ResellPrice} {trade.SourceTicker} per {trade.TargetTicker}.\n");
            }

            WriteSeperator();

            // Gets all available symbols on spot market
            var result = await client.Spot.Market.GetTickersAsync();

            if (!result.Success)
            {
                throw new Exception(result.Error.Message);
            }

            var availableTickers = result.Data.ToList();

            foreach (var trade in trades)
            {
                var task = Task.Run(async () =>
                {
                    WriteLineAsync($"[{trade.Id}] Checking the state of {trade.Symbol}...");

                    // If the symbol is not available, wait until it has been listed on spot market
                    if (!availableTickers.Any(ticker => ticker.Symbol == trade.Symbol))
                    {
                        WriteLineAsync($"[{trade.Id}] {trade.Symbol} is not listed yet. Waiting for the market to open...", ConsoleColor.Yellow);

                        await AwaitListing(trade.Symbol);
                    }

                    WriteLineAsync($"[{trade.Id}] {trade.Symbol} is listed! The market is open. Executing trades...", ConsoleColor.Green);

                    WebCallResult<BinancePlacedOrder> buyOrderCall;

                    do
                    {
                        if (trade.BuyStopPrice.HasValue)
                        {
                            buyOrderCall = await client.Spot.Order.PlaceOrderAsync(trade.Symbol, OrderSide.Buy, OrderType.StopLossLimit, quantity: trade.TargetQuantity, price: trade.MaxBuyPrice, stopPrice: trade.BuyStopPrice, timeInForce: TimeInForce.GoodTillCancel);

                        }
                        else
                        {
                            buyOrderCall = await client.Spot.Order.PlaceOrderAsync(trade.Symbol, OrderSide.Buy, OrderType.Limit, quantity: trade.TargetQuantity, price: trade.MaxBuyPrice, timeInForce: TimeInForce.GoodTillCancel);
                        }

                        if (!buyOrderCall.Success)
                        {
                            WriteLineAsync($"[{trade.Id}] An error occurred while placing the buy order ({buyOrderCall.Error.Message}). New attempt in progress...", ConsoleColor.Red);
                            await Task.Delay(50);
                        }

                    } while (buyOrderCall is null || !buyOrderCall.Success);

                    if (trade.BuyStopPrice.HasValue)
                    {
                        WriteLineAsync($"[{trade.Id}] The buy order has been placed successfully (Symbol: {trade.Symbol}, Side: {OrderSide.Buy}, Type: {OrderType.StopLossLimit}, Quantity: {trade.TargetQuantity}, Price: {trade.MaxBuyPrice}, StopPrice: {trade.BuyStopPrice}).", ConsoleColor.Green);
                    }
                    else
                    {
                        WriteLineAsync($"[{trade.Id}] The buy order has been placed successfully (Symbol: {trade.Symbol}, Side: {OrderSide.Buy}, Type: {OrderType.Limit}, Quantity: {trade.TargetQuantity}, Price: {trade.MaxBuyPrice}).", ConsoleColor.Green);
                    }

                    WebCallResult<BinanceOrder> orderStateCall;

                    decimal quantityPlacedInSalesOrder = 0;

                    do
                    {
                        orderStateCall = await client.Spot.Order.GetOrderAsync(trade.Symbol, orderId: buyOrderCall.Data.OrderId);

                        if (orderStateCall.Success)
                        {
                            decimal quantityToResell = orderStateCall.Data.QuantityFilled - quantityPlacedInSalesOrder;

                            if (quantityToResell > 0)
                            {
                                WriteLineAsync($"[{trade.Id}] {quantityToResell} {trade.TargetTicker} have been purchased. Resale at {trade.ResellPrice} {trade.SourceTicker}...", ConsoleColor.Green);

                                var sellOrderCall = await client.Spot.Order.PlaceOrderAsync(trade.Symbol, OrderSide.Sell, OrderType.Limit, quantity: quantityToResell, price: trade.ResellPrice, timeInForce: TimeInForce.GoodTillCancel);

                                if (sellOrderCall.Success)
                                {
                                    quantityPlacedInSalesOrder = orderStateCall.Data.QuantityFilled;
                                    WriteLineAsync($"[{trade.Id}] The sell order has been placed successfully (Symbol: {trade.Symbol}, Side: {OrderSide.Sell}, Type: {OrderType.Limit}, Quantity: {quantityToResell}, Price: {trade.ResellPrice}).", ConsoleColor.Green);
                                }
                                else
                                {
                                    WriteLineAsync($"[{trade.Id}] An error occurred while placing the sell order ({sellOrderCall.Error.Message}). New attempt in progress...", ConsoleColor.Red);
                                }
                            }
                        }

                        await Task.Delay(100);

                    } while (orderStateCall?.Data?.Status != OrderStatus.Filled);

                    WriteLine($"[{trade.Id}] The trade is now complete!");

                });

                tasks.Add(task);
            }

            await Task.WhenAll(tasks);

            WriteSeperator();

            WriteLine($"All trades have been executed. Well done!");

        }


        private async Task AwaitListing(string symbol)
        {
            if (string.IsNullOrEmpty(symbol))
            {
                throw new ArgumentNullException(nameof(symbol));
            }


            await Task.Run(async () =>
            {
                bool listed = false;

                var ticketUpdatesSubscription = await socket.Spot.SubscribeToBookTickerUpdatesAsync(symbol, (message) =>
                {
                    if (message.Data.Symbol == symbol)
                    {
                        listed = true;
                        return;
                    }
                });

                while (!listed)
                {
                    await Task.Delay(65);

                    var response = await client.Spot.Market.GetPriceAsync(symbol);

                    if (response.Success)
                    {
                        listed = true;
                    }
                }
            });
        }

        private void WriteLineAsync(string message, ConsoleColor color = ConsoleColor.White)
        {
            Task.Run(() =>
            {
                lock (consoleLocker)
                {
                    Console.ForegroundColor = ConsoleColor.White;
                    Console.WriteLine($"[{DateTime.Now}] {message}");
                    Console.ResetColor();
                }
            });
        }

        private void WriteLine(string message, ConsoleColor color = ConsoleColor.White)
        {
            lock (consoleLocker)
            {
                Console.ForegroundColor = ConsoleColor.White;
                Console.WriteLine($"[{DateTime.Now}] {message}");
                Console.ResetColor();
            }
        }

        private void WriteSeperator()
        {
            Console.WriteLine("\n----------------------------------------------------------------------------------------------------------------------------------\n");
        }

        public class CashMachineRunnerBuilder
        {
            private ApiCredentials credentials;
            private readonly List<Allocation> allocations = new List<Allocation>();
            private readonly List<Trade> trades = new List<Trade>();

            public CashMachineRunnerBuilder(string key, string secret)
            {
                Credentials(key, secret);
            }

            public CashMachineRunnerBuilder Credentials(string key, string secret)
            {
                if (string.IsNullOrEmpty(key))
                {
                    throw new ArgumentNullException(nameof(key));
                }

                if (string.IsNullOrEmpty(secret))
                {
                    throw new ArgumentNullException(nameof(secret));
                }

                credentials = new ApiCredentials(key, secret);

                return this;
            }

            public CashMachineRunnerBuilder WithAllocations(params Allocation[] allocations)
            {
                if (allocations is null)
                {
                    throw new ArgumentNullException(nameof(allocations));
                }

                this.allocations.AddRange(allocations);

                return this;
            }

            public CashMachineRunnerBuilder WithTrades(params Trade[] trades)
            {
                if (trades is null)
                {
                    throw new ArgumentNullException(nameof(trades));
                }

                this.trades.AddRange(trades);

                return this;
            }

            public CashMachineRunner Build()
            {
                if (credentials is null)
                {
                    throw new InvalidOperationException("Credentials must be set");
                }

                return new CashMachineRunner(credentials, allocations, trades);
            }
        }
    }
}
