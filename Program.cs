﻿using CashMachine.Models;
using static CashMachine.CashMachineRunner;

// Availables allocations
var allocations = new[]
{
    Allocation.Of("Romuald", "USDT", 20000),
    Allocation.Of("Christopher", "USDT", 20),
    Allocation.Of("Clément", "USDT", 20),
    Allocation.Of("Yohann", "USDT", 20)
};

// Expected trades
var trades = new[]
{
    new Trade()
    {
        Owner = "Romuald",
        SourceQuantity = 10000,
        SourceTicker = "USDT",
        TargetTicker = "LOKA",
        MaxBuyPrice = (decimal)0.16 * 4,
        ResellPrice = (decimal)0.16 * 9,
        Symbol = "LOKAUSDT",
    },
    new Trade()
    {
        Owner = "Romuald",
        SourceQuantity = 5020,
        SourceTicker = "USDT",
        TargetTicker = "LOKA",
        MaxBuyPrice = (decimal)0.16 * 5,
        ResellPrice = (decimal)0.16 * 14,
        Symbol = "LOKAUSDT",
        BuyStopPrice = (decimal) (0.16 * 4.5),
    },
    new Trade()
    {
        Owner = "Romuald",
        SourceQuantity = 3020,
        SourceTicker = "USDT",
        TargetTicker = "LOKA",
        MaxBuyPrice = (decimal)0.16 * 6,
        ResellPrice = (decimal)0.16 * 20,
        Symbol = "LOKAUSDT",
        BuyStopPrice =  (decimal) (0.16 * 5.5),
    },
    new Trade()
    {
        Owner = "Romuald",
        SourceQuantity = 2020,
        SourceTicker = "USDT",
        TargetTicker = "LOKA",
        MaxBuyPrice = (decimal)0.16 * 7,
        ResellPrice = (decimal)0.16 * 30,
        Symbol = "LOKAUSDT",
        BuyStopPrice = (decimal) (0.16 * 6.5),
    },
};


var runner = new CashMachineRunnerBuilder("Key", "Secret")
    .WithAllocations(allocations)
    .WithTrades(trades)
    .Build();



await runner.RunAsync();
