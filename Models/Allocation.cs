﻿namespace CashMachine.Models
{
    public record Allocation
    {
        public string Owner { get; init; }

        public string Symbol { get; init; }

        public decimal Quantity { get; init; }

        private Allocation()
        {
        }

        public static Allocation Of(string owner, string symbol, decimal quantity)
        {
            if (string.IsNullOrEmpty(owner))
            {
                throw new ArgumentNullException(nameof(owner));
            }

            if (string.IsNullOrEmpty(symbol))
            {
                throw new ArgumentNullException(nameof(symbol));
            }

            if (quantity < 0)
            {
                throw new ArgumentException(nameof(quantity));
            }

            return new Allocation()
            {
                Symbol = symbol,
                Quantity = quantity,
            };
        }
    }
}
