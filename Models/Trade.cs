﻿namespace CashMachine.Models
{
    public record Trade
    {
        public string Id { get; } = Guid.NewGuid().ToString();

        public string Owner { get; init; }

        public string SourceTicker { get; init; }

        public decimal SourceQuantity { get; init; }

        public string TargetTicker { get; init; }

        public decimal TargetQuantity => Math.Round(SourceQuantity / MaxBuyPrice, 4);

        public decimal MaxBuyPrice { get; init; }

        public decimal? BuyStopPrice { get; init; }

        public decimal ResellPrice { get; init; }

        public string Symbol { get; init; }
    }
}
